"""
Program for demonstrating the formation of UAVs.
This program is expanded from pyparrot authored by Amy McGovern.

Author:
Hilton Tnunay
hilton.tnunay@manchester.ac.uk
"""

import time
from pyparrot.Mambo import Mambo

class state:
    def __init__(self):
        self.roll = 0.0
        self.pitch = 0.0
        self.yaw = 0.0
        self.altitude = 0.0

intern_sensor = state()
extern_sensor = state()

mamboAddr = "F0:03:8C:2F:5E:4D"
# mamboAddr = "00:13:EF:6C:0A:AE"
mambo = Mambo(mamboAddr, use_wifi=True)

print("Setting up connection with Mambo.")
success = mambo.connect(num_retries=3)
print("Connection status: %s" % success)
mambo.smart_sleep(2)

# get the state information
print("Enabling sensor update from Mambo.")
mambo.ask_for_state_update()
mambo.smart_sleep(2)

max_flying_time = 10
operation_flag = 'landed'
start_time = 0
flying_time = 0

f = open('./data/sensor.txt', 'w')

if(success):
    while(1):
        if(operation_flag == 'landed'):
            if(flying_time <= max_flying_time):
                start_time = time.time()
                mambo.safe_takeoff(5)
                mambo.smart_sleep(1)
                operation_flag = 'flying'
                print("Status: %s." % mambo.sensors.flying_state)
        if(operation_flag == 'flying'):
            flying_time = time.time()-start_time
            if(flying_time <= max_flying_time):
                batt_level = mambo.sensors.battery
                (intern_sensor.roll, intern_sensor.pitch, intern_sensor.yaw, intern_sensor.altitude) = mambo.sensors.get_estimated_states()
                print("Time: %s | Batt: %s | Roll: %s | Pitch: %s | Yaw: %s | Altitude: %s." % (flying_time, batt_level, intern_sensor.roll, -intern_sensor.pitch, intern_sensor.yaw, intern_sensor.altitude))                
                f.write("Time: %s | Batt: %s | Roll: %s | Pitch: %s | Yaw: %s | Altitude: %s.\n" % (flying_time, batt_level, intern_sensor.roll, -intern_sensor.pitch, intern_sensor.yaw, intern_sensor.altitude))                
                mambo.fly_direct(roll=0, pitch=0, yaw=0, vertical_movement=0, duration=0)
            if(flying_time > max_flying_time):
                mambo.safe_land(5)
                mambo.smart_sleep(1)
                operation_flag = 'landed'
                print("Status: %s." % mambo.sensors.flying_state)
                mambo.smart_sleep(2)
                print("Disconnecting.")
                mambo.disconnect()
                print("Disconnected.")
